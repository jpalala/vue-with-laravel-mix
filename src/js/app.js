import Vue from 'vue';
import Notification from './components/Notification.vue';

//Vue.component('notification', require('./components/Notification.vue'));

new Vue({
  el: '#app',
  components: {Notification}
   data: {
        greeting: 'Welcome to your Vue.js app!',
      },
      methods: {
        humanizeURL: function (url) {
          return url
            .replace(/^https?:\/\//, '')
            .replace(/\/$/, '')
        }
   }
 
});
