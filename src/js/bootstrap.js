window.Vue = require('vue');

window.events = new Vue();

window.showNotification = function(message, type = 'alert-primary') {
      window.events.$emit('showNotification', message, type);
}
